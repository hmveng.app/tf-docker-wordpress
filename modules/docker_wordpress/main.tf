terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

resource "docker_volume" "data_vol" {
  name   = "wp_vol"
  driver = "local"
  driver_opts = {
    o      = "bind"
    type   = "none"
    device = "${path.cwd}/db/"
  }
}

resource "docker_network" "wp_net" {
  name = "wp_net"
}

data "docker_registry_image" "mysql" {
  name = "mysql:8.3.0"
}

resource "docker_image" "mysql" {
  name          = data.docker_registry_image.mysql.name
  pull_triggers = [data.docker_registry_image.mysql.sha256_digest]
}

data "docker_registry_image" "wp" {
  name = "wordpress:latest"
}

resource "docker_image" "wp" {
  name          = data.docker_registry_image.wp.name
  pull_triggers = [data.docker_registry_image.wp.sha256_digest]
}

resource "docker_container" "wp_db" {
  image        = docker_image.mysql.image_id
  name         = var.db_container_name
  restart      = "always"
  network_mode = docker_network.wp_net.name
  networks_advanced {
    name = docker_network.wp_net.name
  }
  env = [
    "MYSQL_ROOT_PASSWORD=${var.db_pwd_root}",
    "MYSQL_PASSWORD=${var.db_pwd}",
    "MYSQL_USER=${var.db_user}",
    "MYSQL_DATABASE=${var.db_name}",
  ]
  volumes {
    container_path = "/var/lib/mysql/"
    volume_name    = docker_volume.data_vol.name
  }
}

resource "docker_container" "wp" {
  image        = docker_image.wp.image_id
  name         = var.wp_container_name
  restart      = "always"
  network_mode = docker_network.wp_net.name
  working_dir  = "/var/www/html"
  networks_advanced {
    name = docker_network.wp_net.name
  }
  env = [
    "WORDPRESS_DB_HOST=${docker_container.wp_db.name}:3306",
    "WORDPRESS_DB_USER=${var.db_user}",
    "WORDPRESS_DB_PASSWORD=${var.db_pwd}",
    "WORDPRESS_DB_NAME=${var.db_name}"
  ]
  ports {
    internal = 80
    external = var.wp_port
  }
  depends_on = [docker_container.wp_db]
}