variable "db_container_name" {
  type    = string
  default = "wordpress_db"
}

variable "wp_container_name" {
  type    = string
  default = "wordpress_app"
}

variable "db_pwd_root" {
  type = string
}

variable "db_pwd" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_name" {
  type = string
}

variable "wp_port" {
  type = number
}