
module "install_wp" {
  source      = "./modules/docker_wordpress"
  db_pwd_root = var.wordpress_db_pwd_root
  db_pwd      = var.wordpress_db_pwd
  db_user     = var.wordpress_db_user
  db_name     = var.wordpress_db_name
  wp_port     = var.wordpress_port
}