variable "wordpress_db_pwd_root" {
  type = string
}

variable "wordpress_db_pwd" {
  type = string
}

variable "wordpress_db_user" {
  type = string
}

variable "wordpress_db_name" {
  type = string
}

variable "wordpress_port" {
  type = number
}