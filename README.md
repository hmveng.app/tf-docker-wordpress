# Todo API

This is wordpress app running in the terraform infrastructure provosionning by docker

## How to run app

create terraform.tfvars file with the followings varaibles:
```bash
wordpress_db_pwd_root = ""
wordpress_db_pwd      = ""
wordpress_db_user     = ""
wordpress_db_name     = ""
wordpress_port        = 
```

And run this commands
```bash
terraform get
terraform init
terraform plan
terraform apply
```

open your browser http://your_ip:wordpress_port